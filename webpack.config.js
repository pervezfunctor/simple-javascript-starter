const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devtool: 'cheap-module-eval-source-map',
  output: {
    publicPath: '/build/',
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundle.js',
  },
  module: {
    rules: [{ test: /\.jsx?$/, loader: 'babel-loader' }],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      utils$: path.resolve(__dirname, './src/utils.js'),
    },
  },
  devServer: {
    contentBase: 'build',
    open: true,
    openPage: 'build/index.html',
    hot: true,
    noInfo: true,
    overlay: {
      errors: true,
      warnings: true,
    },
  },
  watchOptions: { ignored: ['build', 'node_modules'] },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Simple Javascript Starter',
      template: 'index.html',
    }),
  ],
}
